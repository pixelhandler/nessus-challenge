# Nessus Challenge

This repository contains two projects one as the API and the other as the UI.

To run the applications:

*Start the API server*

1. `cd nessus-api`
2. `npm install`
3. `npm run start`

The API server will run with the default port of 3000.

*Start the UI application*

1. `cd nessus-ui`
2. `yarn install`
3. `yarn start`

Since the API is already running on port 3000 answer 'Y'
start the UI application using port 3001.

The browser should launch the application at http://localhost:3001
with a local proxy to the API at http://localhost:3000


## Solution

### Backend

The API server is just a mock list of `hosts`

GET `/download/request?host=${count}`

```javascript
[{
  name: `host${i + 1}`,
  hostname: `nessus-${isEven ? 'xml' : 'ntp'}.lab.com`,
  port: isEven ? 3384 : 1241,
  username: isEven ? "admin" : "toto"
}]
```

### Frontend

To run UI tests for the components use `yarn test`.

- The UI application is a React.js application
- Styles use [basscss](http://basscss.com/) a utility library


## Additional Info

Since the API can return 10,000 using the `host` param, there is room to optimize.

*Considerations*

1. Will pagination work?
2. Alternatively instead of a form to choose number of hosts a load more button
   could be added to the end to the list which loads in smaller batches.
3. Likewise since the host resource is small, loading 10,000 does work, but
   that is a bit much for the user interface.
   - Could render a sparse collection for only the items that fit on the screen.
