let express = require('express');
let router = express.Router();

router.get('/request', function(req, res) {
  if (req.accepts('application/json') && req.query.host) {
    let count = parseInt(req.query.host, 10);
    res.status(200).json(mockHosts(count));
  } else {
    res.status(400).send(`{"error": "Bad Request"}`);
  }
});

module.exports = router;

function mockHosts(count) {
  return [...(new Array(count)).keys()].map((i) => {
    let isEven = i % 2 === 0;
    return {
      name: `host${i + 1}`,
      hostname: `nessus-${isEven ? 'xml' : 'ntp'}.lab.com`,
      port: isEven ? 3384 : 1241,
      username: isEven ? "admin" : "toto"
    };
  });
}

