import React, { Component } from 'react';
import HostsForm from './components/HostsForm';
import HostsList from './components/HostsList';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: '10',
      hosts: [],
      error: '',
    }
    this.handleCountChange = this.handleCountChange.bind(this);
  }

  styles() {
    return {
      header: 'p2 border-none rounded bg-navy',
      container: 'm1',
      title: 'p0 m0 center white',
      main: 'mx1 my2 flex flex-wrap content-center',
    };
  }

  componentDidMount() {
    this.fetchHosts();
  }

  fetchHosts(count) {
    count = count || this.state.count;
    const url = `/download/request?host=${count}`;
    return axios.get(url)
      .then(response => {
        const hosts = response.data;
        this.setState({ hosts });
      })
      .catch(e => {
        const error = e.error;
        this.setState({ error });
      });
  }

  handleCountChange(count) {
    this.setState({ count });
    return this.fetchHosts(count);
  }

  render() {
    const styles = this.styles();
    let main;
    if (this.state.error) {
      main = (
        <main className={styles.main}>
          <p>{this.state.error}</p>
        </main>
      );
    } else {
      main = (
        <main className={styles.main}>
          <HostsForm count={this.state.count} handleCountChange={this.handleCountChange} />
          <HostsList hosts={this.state.hosts} />
        </main>
      );
    }
    return (
      <div id="App" className={styles.container}>
        <header className={styles.header}>
          <h1 className={styles.title}>Nessus UI</h1>
        </header>
        {main}
      </div>
    );
  }
}

export default App;
