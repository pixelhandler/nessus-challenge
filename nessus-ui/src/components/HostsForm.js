import React, { Component } from 'react';

class HostsForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      count: props.count || '2',
      inFlight: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ count: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ inFlight: true });
    this.props.handleCountChange(this.state.count)
      .finally(() => this.setState({ inFlight: false }));
  }

  styles() {
    return {
      form: 'col-12 center',
      input: 'mx2',
      submit: 'm0 px2 py1 white bg-blue border-none rounded',
    };
  }

  render() {
    const styles = this.styles();
    return (
      <form onSubmit={this.handleSubmit} className={styles.form}>
        <label>
          Enter number of hosts:
          <input type="number"
            className={styles.input}
            min="1"
            max="10000"
            value={this.state.count}
            onChange={this.handleChange} />
        </label>
        <button type="submit"
          disabled={this.state.inFlight}
          className={styles.submit}>Sumit</button>
      </form>
    );
  }
}

export default HostsForm;

