import React from 'react';
import { mount } from 'enzyme';
import HostsList from './HostsList';

const emptyMessage = 'No hosts yet.';

it('renders hosts list with empty message', () => {
  const hosts = [];
  const wrapper = mount(<HostsList hosts={hosts} />);
  expect(wrapper.find('table')).toExist();
  const el = wrapper.find('table > tbody');
  expect(el).toIncludeText(emptyMessage);
});

it('renders hosts list items', () => {
  const hosts = [{
    name: 'Bill',
    hostname: 'pixelhandler.com',
    port: 80,
    username: 'secret',
  }];

  const wrapper = mount(<HostsList hosts={hosts} />);

  expect(wrapper.find('table')).toExist();
  expect(wrapper.find('table > thead')).toExist();
  expect(wrapper.find('table > tbody')).toExist();
  const el = wrapper.find('table > tbody');
  expect(el).not.toIncludeText(emptyMessage);
  const body = wrapper.find('table > tbody').children();
  ['name', 'hostname', 'port', 'username'].map((prop) => {
    expect(body.find(`#${prop}-0`)).toIncludeText(hosts[0][prop]);
  });
  wrapper.unmount();
});

