import React from 'react';
import { shallow } from 'enzyme';
import HostsForm from './HostsForm';

it('renders hosts form', () => {
  const wrapper = shallow(<HostsForm />);
  expect(wrapper.find('form > label > input')).toExist();
  expect(wrapper.find('label')).toIncludeText('Enter number of hosts');
  expect(wrapper.find('form > button')).toExist();
});

