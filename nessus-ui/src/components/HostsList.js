import React from 'react';
import './HostsList.css';

function HostsList(props) {
  const styles = props.styles || {
    list: 'my1 p1 col-12 table-light',
    item: 'my1 p1 col-3 left-align',
  };
  return (
    <table className={styles.list}>
      <thead>
        <TableHead hosts={props.hosts} styles={styles} />
      </thead>
      <tbody>
        <TableBody hosts={props.hosts} styles={styles} />
      </tbody>
    </table>
  );
}

function TableHead(props) {
  const styles = Object.assign({}, {
    heading: 'regular left-align'
  }, props.styles);
  if (hasHosts(props)) {
    return (
      <tr className={styles.item}>
        <th className={styles.heading}>Name</th>
        <th className={styles.heading}>Hostname</th>
        <th className={styles.heading}>Port</th>
        <th className={styles.heading}>Username</th>
      </tr>
    );
  } else {
    return null;
  }
}

function TableBody(props) {
  const styles = Object.assign({}, {
    data: 'p1',
  }, props.styles);
  if (hasHosts(props)) {
    return props.hosts.map((host, index) => {
      return (
        <tr className={styles.item} key={index}>
          <td className={styles.data} id={'name-'+index}>{host.name}</td>
          <td className={styles.data}  id={'hostname-'+index}>{host.hostname}</td>
          <td className={styles.data}  id={'port-'+index}>{host.port}</td>
          <td className={styles.data}  id={'username-'+index}>{host.username}</td>
        </tr>
      );
    });
  } else {
    let empty = styles.data + ' center';
    return (
      <tr className={styles.item}>
        <td colSpan="4" className={empty}>
          <em>No hosts yet.</em>
        </td>
      </tr>
    );
  }
}

function hasHosts(props) {
  return props.hosts && props.hosts.length > 0;
}

export default HostsList;

