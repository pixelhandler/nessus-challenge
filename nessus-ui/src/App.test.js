import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

it('renders app title', () => {
  const wrapper = shallow(<App />);
  expect(wrapper.find('h1')).toIncludeText('Nessus UI');
  expect(wrapper.find('#App')).toHaveClassName('m1');
  expect(wrapper.find('#App > header')).toHaveClassName('p2 border-none rounded bg-navy');
  expect(wrapper.find('#App > header > h1')).toHaveClassName('p0 m0 center white');
  expect(wrapper.find('#App > main')).toHaveClassName('mx1 my2 flex flex-wrap content-center');
});

